//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Marketing.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class MR_GetDetailKomponenKontrakMCU
    {
        public short CustomerID { get; set; }
        public string IDJasa { get; set; }
        public string IDJasaInclude { get; set; }
        public string IDKomponenID { get; set; }
        public decimal Harga { get; set; }
        public string KomponenName { get; set; }
    }
}
