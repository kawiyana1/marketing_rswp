//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Marketing.Entities.SIM
{
    using System;
    using System.Collections.Generic;
    
    public partial class MR_ListTarifKerjasama
    {
        public string Customer_Kode_Customer { get; set; }
        public string Nama_Customer { get; set; }
        public string JenisKerjasama_JenisKerjasama { get; set; }
        public string Kelas_NamaKelas { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public decimal CoPay { get; set; }
        public short MaxHariRawatPerOpname { get; set; }
        public short MaxHariRawatPerTahun { get; set; }
        public string KelebihanPlafon { get; set; }
        public decimal CustomerKerjasamaID { get; set; }
    }
}
