﻿using Marketing.Entities.SIM;
using Marketing.Helper;
using Marketing.Models.Kerjasama;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Marketing.Controllers.Kerjasama
{
    [Authorize(Roles = "Marketing")]
    public class SetupKontrakKerjasamaController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_ListKontrakKerjasama.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Customer_Kode_Customer.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value) ||
                                y.Kelas_NamaKelas.Contains(x.Value) ||
                                y.JenisKerjasama_JenisKerjasama.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        CustomerKerjasamaID = m.CustomerKerjasamaID,
                        Kode = m.Customer_Kode_Customer,
                        Nama = m.Nama_Customer,
                        JenisKerjasama = m.JenisKerjasama_JenisKerjasama,
                        Kelas = m.Kelas_NamaKelas,
                        StartDate = m.StartDate.ToString("yyyy/MM/dd"),
                        EndDate = m.EndDate.ToString("yyyy/MM/dd"),
                        CoPay = m.CoPay,
                        MaxHariRawatPerOpname = m.MaxHariRawatPerOpname,
                        MaxHariRawatPerTahun = m.MaxHariRawatPerTahun,
                        KelebihanPlafon = m.KelebihanPlafon
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, SetupKontrakKerjasamaModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        var id = model.IdPerusahaan;
                        if (_process == "CREATE")
                        {
                            if (model.DetailPlafon == null) model.DetailPlafon = new List<SetupKontrakKerjasamaDetailPlafonModel>();
                            if (model.Formularium == null) model.Formularium = new List<SetupKontrakKerjasamaFormulariumModel>();
                            if (model.Diskon == null) model.Diskon = new List<SetupKontrakKerjasamaDiskonModel>();
                            if (model.SectionDitanggung == null) model.SectionDitanggung = new List<SetupKontrakKerjasamaSectionDitanggungModel>();
                            if (model.JasaExectionDiskon == null) model.JasaExectionDiskon = new List<SetupKontrakKerjasamaJasaExectionDiskonModel>();
                            if (model.RasioObat == null) model.RasioObat = new List<SetupKontrakKerjasamaRasioObatModel>();
                            if (model.Markup == null) model.Markup = new List<SetupKontrakKerjasamaMarkupModel>();
                            if (model.DataPaketObat == null) model.DataPaketObat = new List<SetupKontrakKerjasamaDataPaketObatModel>();

                            s.MR_InsertHeaderKontrakKerjasama(
                                model.IdPerusahaan,
                                model.JenisKerjasama,
                                model.KelasPelayanan,
                                model.KelasKontrak,
                                model.StartDate,
                                model.EndDate,
                                model.CoPay,
                                model.Ditanggung,
                                model.KelebihanPlafon,
                                model.IuranPerAnggota,
                                model.TglJatuhTempo,
                                model.JenisFormularium,
                                model.MaxRawatOpname1,
                                model.MaxRawatOpname2,
                                model.MaxICUOpname1,
                                model.MaxICUOpname2,
                                model.MaxNilaiRawatInap,
                                model.TarifYangDigunakanUp,
                                model.TarifYangDigunakanDiskon == null ? 0 : model.TarifYangDigunakanDiskon,
                                model.DiskonLangsungKePasien == null ? false : model.DiskonLangsungKePasien,
                                model.HargaObatDigunakan,
                                model.UpObat,
                                model.DiskonObat,
                                model.JenisPembayaran,
                                model.JenisPembayaranYangDitanggung == null ? 0 : model.JenisPembayaranYangDitanggung,
                                model.PaketKerjasama == null ? "0" : model.PaketKerjasama,
                                model.Aktif,
                                model.SembunyikanObat,
                                model.PaketPemeriksaan
                            ).FirstOrDefault();

                            foreach (var x in model.DetailPlafon)

                            {
                                s.MR_InsertDetailPlafon(
                                    model.CustomerKerjasamaID,
                                    x.KategoriPlafon,
                                    x.Ditanggung,
                                    x.NilaiPlafon
                                );
                            }

                            foreach (var x in model.Formularium)
                            {
                                s.MR_InsertFormulariumKontrakKerjasama(
                                   model.CustomerKerjasamaID,
                                    x.Id,
                                    null,
                                    x.Formularium,
                                    x.Ditanggung
                                );
                            }

                            foreach (var x in model.Diskon)
                            {
                                s.MR_InsertDiscountKontrakKerjasama(
                                    model.CustomerKerjasamaID,
                                    x.Diskon,
                                    null,
                                    null
                                );
                            }

                            foreach (var x in model.SectionDitanggung)
                            {
                                s.MR_InsertSectionTdkDitanggung(
                                    model.CustomerKerjasamaID,
                                    x.Id,
                                    x.DitanggungSection
                                );
                            }

                            foreach (var x in model.JasaExectionDiskon)
                            {
                                s.MR_InsertJasaExceptionDisc(
                                    model.CustomerKerjasamaID,
                                    x.Id
                                );
                            }

                            foreach (var x in model.RasioObat)
                            {
                                s.MR_InsertRasioObatKontrakKerjasama(
                                    model.CustomerKerjasamaID,
                                    x.Spesialis,
                                    x.SubSpesialis,
                                    x.NilaiRasio,
                                    x.Blok
                                );
                            }

                            foreach (var x in model.Markup)
                            {
                                s.MR_InsertMarkUptKontrakKerjasama(
                                    model.CustomerKerjasamaID,
                                    x.GroupJasa,
                                    x.Markup,
                                    x.Keterangan
                                );
                            }

                            foreach (var x in model.DataPaketObat)
                            {
                                s.MR_InsertDataPaketObat(
                                    model.CustomerKerjasamaID,
                                    x.KelompokJenis
                                );
                            }

                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            #region Header
                            s.MR_UpdateHeaderKontrakKerjasama(
                                model.CustomerKerjasamaID,
                                model.IdPerusahaan,
                                model.JenisKerjasama,
                                model.KelasPelayanan,
                                model.KelasKontrak,
                                model.StartDate,
                                model.EndDate,
                                model.CoPay,
                                model.Ditanggung,
                                model.KelebihanPlafon,
                                model.IuranPerAnggota,
                                model.TglJatuhTempo,
                                model.JenisFormularium,
                                model.MaxRawatOpname1,
                                model.MaxRawatOpname2,
                                model.MaxICUOpname1,
                                model.MaxICUOpname2,
                                model.MaxNilaiRawatInap,
                                model.TarifYangDigunakanUp,
                                model.TarifYangDigunakanDiskon,
                                model.DiskonLangsungKePasien,
                                model.HargaObatDigunakan,
                                model.UpObat,
                                model.DiskonObat,
                                model.JenisPembayaran,
                                model.JenisPembayaranYangDitanggung,
                                model.PaketKerjasama == null ? "0" : model.PaketKerjasama,
                                model.Aktif,
                                model.SembunyikanObat,
                                model.PaketPemeriksaan
                            );
                            #endregion

                            #region DetailPlafon

                            var d_DetailPlafon = s.MR_GetDetailPlafon.Where(x => x.CustomerKerjasamaID == model.CustomerKerjasamaID).ToList();
                            if (d_DetailPlafon.Count() > 0)
                            {
                                foreach (var x in d_DetailPlafon)
                                {
                                    var _d = model.DetailPlafon.FirstOrDefault(y => y.KategoriPlafon == x.KategoriPlafon);
                                    //delete
                                    //if (_d == null) s.INV_DeleteGudangAmprahanDetail(model.IdPerusahaan, x.KategoriPlafon);
                                }
                                foreach (var x in model.DetailPlafon)
                                {
                                    var _d = d_DetailPlafon.FirstOrDefault(y => y.KategoriPlafon == x.KategoriPlafon);
                                    if (_d == null)
                                    {
                                        // new
                                        s.MR_InsertDetailPlafon(
                                            model.CustomerKerjasamaID,
                                            x.KategoriPlafon,
                                            x.Ditanggung,
                                            x.NilaiPlafon
                                        );
                                    }
                                    else
                                    {
                                        // edit
                                        //s.INV_UpdateGudangAmprahanDetail(
                                        //    model.IdPerusahaan,
                                        //    x.KategoriPlafon,
                                        //    x.Ditanggung,
                                        //    x.NilaiPlafon
                                        //);
                                    }
                                }
                            }


                            #endregion

                            #region Formularium

                            var d_Formularium = s.MR_GetFormularium.Where(x => x.CustomerKerjasamaID == model.CustomerKerjasamaID).ToList();
                            if (d_DetailPlafon.Count() > 0)
                            {
                                foreach (var x in d_Formularium)
                                {
                                    var _d = model.Formularium.FirstOrDefault(y => y.BarangID == x.Barang_ID);
                                    //delete
                                    //if (_d == null) s.INV_DeleteGudangAmprahanDetail(model.IdPerusahaan, x.KategoriPlafon);
                                }
                                foreach (var x in model.Formularium)
                                {
                                    var _d = d_Formularium.FirstOrDefault(y => y.Barang_ID == x.BarangID);
                                    if (_d == null)
                                    {
                                        // new
                                        s.MR_InsertFormulariumKontrakKerjasama(
                                           model.CustomerKerjasamaID,
                                           x.BarangID,
                                           Convert.ToInt16(model.JenisKerjasama),
                                           x.Formularium,
                                           x.Ditanggung
                                       );
                                    }
                                    else
                                    {
                                        //edit
                                        s.MR_UpdateFormulariumKontrakKerjasama(
                                            model.CustomerKerjasamaID,
                                            x.BarangID,
                                            x.JenisKerjasamaID,
                                            x.Include,
                                            x.DitanggungFormularium
                                        );
                                    }
                                }
                            }

                            #endregion

                            #region Diskon

                            var d_Diskon = s.MR_GetDetailDiscount.Where(x => x.CustomerKerjasamaID == model.CustomerKerjasamaID).ToList();
                            if (d_DetailPlafon.Count() > 0)
                            {
                                foreach (var x in d_Diskon)
                                {
                                    var _d = model.Diskon.FirstOrDefault(y => y.Diskon == x.IDDiscount);
                                    //delete
                                    //if (_d == null) s.INV_DeleteGudangAmprahanDetail(model.IdPerusahaan, x.KategoriPlafon);
                                }
                                foreach (var x in model.Diskon)
                                {
                                    var _d = d_Diskon.FirstOrDefault(y => y.IDDiscount == x.Diskon);
                                    if (_d == null)
                                    {
                                        // new
                                        s.MR_InsertDiscountKontrakKerjasama(
                                            model.CustomerKerjasamaID,
                                            x.Diskon,
                                            x.DiscRI,
                                            x.DIscRJ
                                        );
                                    }
                                    else
                                    {
                                        // edit
                                        s.MR_UpdateDiscountKontrakKerjasama(
                                            model.CustomerKerjasamaID,
                                            x.IDDiscount,
                                            x.Nilai,
                                            x.NilaiRJ
                                        );
                                    }
                                }
                            }


                            #endregion

                            #region SectionDitanggung

                            var d_SectionDitanggung = s.MR_GetSectionTidakDitanggung.Where(x => x.CustomerKerjasamaID == model.CustomerKerjasamaID).ToList();
                            if (d_DetailPlafon.Count() > 0)
                            {

                                foreach (var x in d_SectionDitanggung)
                                {
                                    var _d = model.SectionDitanggung.FirstOrDefault(y => y.Id == x.Lokasi_ID);
                                    //delete
                                    //if (_d == null) s.INV_DeleteGudangAmprahanDetail(model.IdPerusahaan, x.KategoriPlafon);
                                }
                                foreach (var x in model.SectionDitanggung)
                                {
                                    var _d = d_SectionDitanggung.FirstOrDefault(y => y.Lokasi_ID == x.Id);
                                    if (_d == null)
                                    {
                                        // new
                                        s.MR_InsertSectionTdkDitanggung(
                                             model.CustomerKerjasamaID,
                                            x.Id,
                                            x.DitanggungSection
                                        );
                                    }
                                    //else
                                    //{
                                    // edit
                                    //s.MR_UpdateSectionTdkDitanggung(
                                    //    model.CustomerKerjasamaID,
                                    //    x.Id,
                                    //x.DitanggungSection tipe data tidak sesuai
                                    //);
                                    //}
                                }
                            }

                            #endregion

                            #region JasaExectionDiskon

                            var d_JasaExectionDiskon = s.MR_GetDetailJasaExceptionDisc.Where(x => x.CustomerKerjasamaID == model.CustomerKerjasamaID).ToList();
                            if (d_DetailPlafon.Count() > 0)
                            {
                                foreach (var x in d_JasaExectionDiskon)
                                {
                                    var _d = model.JasaExectionDiskon.FirstOrDefault(y => y.Id == x.JasaID);
                                    //delete
                                    //if (_d == null) s.INV_DeleteGudangAmprahanDetail(model.IdPerusahaan, x.KategoriPlafon);
                                }
                                foreach (var x in model.JasaExectionDiskon)
                                {
                                    var _d = d_JasaExectionDiskon.FirstOrDefault(y => y.JasaID == x.Id);
                                    if (_d == null)
                                    {
                                        // new
                                        s.MR_InsertJasaExceptionDisc(
                                            model.CustomerKerjasamaID,
                                            x.Id
                                        );
                                    }
                                    else
                                    {
                                        // edit
                                        s.MR_UpdateJasaExceptionDisc(
                                            model.CustomerKerjasamaID,
                                            x.Id
                                        );
                                    }
                                }
                            }

                            #endregion

                            #region RasioObat

                            var d_RasioObat = s.MR_GetRasioObat.Where(x => x.CustomerKerjasamaID == model.IdPerusahaan).ToList();
                            if (d_DetailPlafon.Count() > 0)
                            {
                                foreach (var x in d_RasioObat)
                                {
                                    var _d = model.RasioObat.FirstOrDefault(y => y.Spesialis == x.SpesialisID);
                                    //delete
                                    //if (_d == null) s.INV_DeleteGudangAmprahanDetail(model.IdPerusahaan, x.KategoriPlafon);
                                }
                                foreach (var x in model.RasioObat)
                                {
                                    var _d = d_RasioObat.FirstOrDefault(y => y.SpesialisID == x.Spesialis);
                                    if (_d == null)
                                    {
                                        // new
                                        s.MR_InsertRasioObatKontrakKerjasama(
                                            model.CustomerKerjasamaID,
                                            x.Spesialis,
                                            x.SubSpesialis,
                                            x.NilaiRasio,
                                            x.Blok
                                        );
                                    }
                                    else
                                    {
                                        // edit
                                        s.MR_UpdateRasioObatKontrakKerjasama(
                                            model.CustomerKerjasamaID,
                                            x.Spesialis,
                                            x.SubSpesialis,
                                            x.NilaiRasio,
                                            x.Blok
                                        );
                                    }
                                }
                            }

                            #endregion

                            #region Markup

                            var d_Markup = s.MR_GetDetailMarkUp.Where(x => x.CustomerKerjasamaID == model.CustomerKerjasamaID).ToList();
                            if (d_DetailPlafon.Count() > 0)
                            {
                                foreach (var x in d_Markup)
                                {
                                    var _d = model.Markup.FirstOrDefault(y => y.GroupJasa == x.GroupJasaID);
                                    //delete
                                    //if (_d == null) s.INV_DeleteGudangAmprahanDetail(model.IdPerusahaan, x.KategoriPlafon);
                                }
                                foreach (var x in model.Markup)
                                {
                                    var _d = d_Markup.FirstOrDefault(y => y.GroupJasaID == x.GroupJasa);
                                    if (_d == null)
                                    {
                                        // new
                                        s.MR_InsertMarkUptKontrakKerjasama(
                                            model.CustomerKerjasamaID,
                                            x.GroupJasa,
                                            x.Markup,
                                            x.Keterangan
                                        );
                                    }
                                    else
                                    {
                                        // edit
                                        s.MR_UpdateMarkUptKontrakKerjasama(
                                            model.CustomerKerjasamaID,
                                            x.GroupJasa,
                                            x.Markup,
                                            x.Keterangan
                                        );
                                    }
                                }
                            }

                            #endregion

                            #region DataPaketObat

                            var d_DataPaketObat = s.MR_GetDetailDataPaketObat.Where(x => x.CustomerKerjasamaID == model.CustomerKerjasamaID).ToList();
                            if (d_DetailPlafon.Count() > 0)
                            {
                                foreach (var x in d_DataPaketObat)
                                {
                                    var _d = model.DataPaketObat.FirstOrDefault(y => y.KelompokJenis == x.KelompokJenisObat);
                                    //delete
                                    //if (_d == null) s.INV_DeleteGudangAmprahanDetail(model.IdPerusahaan, x.KategoriPlafon);
                                }
                                foreach (var x in model.DataPaketObat)
                                {
                                    var _d = d_DataPaketObat.FirstOrDefault(y => y.KelompokJenisObat == x.KelompokJenis);
                                    if (_d == null)
                                    {
                                        // new
                                        s.MR_InsertDataPaketObat(
                                            model.CustomerKerjasamaID,
                                            x.KelompokJenis
                                        );
                                    }
                                    else
                                    {
                                        // edit
                                        s.MR_UpdateDataPaketObat(
                                            model.CustomerKerjasamaID,
                                            x.KelompokJenis
                                        );
                                    }
                                }
                            }
                           

                            #endregion

                            s.SaveChanges();
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupSetupKontrakKerjasama-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(decimal id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetHeaderKontrakKerjasama.FirstOrDefault(x => x.CustomerKerjasamaID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d_DetailPlafon = s.MR_GetDetailPlafon.Where(x => x.CustomerKerjasamaID == id).ToList();
                    var d_Formularium = s.MR_GetFormularium.Where(x => x.CustomerKerjasamaID == id).ToList();
                    var d_Diskon = s.MR_GetDetailDiscount.Where(x => x.CustomerKerjasamaID == id).ToList();
                    var d_SectionDitanggung = s.MR_GetSectionTidakDitanggung.Where(x => x.CustomerKerjasamaID == id).ToList();
                    var d_JasaExectionDiskon = s.MR_GetDetailJasaExceptionDisc.Where(x => x.CustomerKerjasamaID == id).ToList();
                    var d_RasioObat = s.MR_GetRasioObat.Where(x => x.CustomerKerjasamaID == id).ToList();
                    var d_Markup = s.MR_GetDetailMarkUp.Where(x => x.CustomerKerjasamaID == id).ToList();
                    var d_DataPaketObat = s.MR_GetDetailDataPaketObat.Where(x => x.CustomerKerjasamaID == id).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            IdPerusahaan = m.CustomerID,
                            CustomerKerjasamaID = m.CustomerKerjasamaID,
                            KodePerusahaan = m.Kode_Customer,
                            NamaPerusahaan = m.Nama_Customer,
                            AlamatPerusahaan = m.Alamat_1,
                            JenisKerjasama = m.JenisKerjasamaID,
                            KelasPelayanan = m.KelasID,
                            KelasKontrak = m.KelasKontrak,
                            StartDate = m.StartDate.ToString("yyyy-MM-dd"),
                            EndDate = m.StartDate.ToString("yyyy-MM-dd"),
                            CoPay = m.CoPay,
                            Ditanggung = m.Ditanggung,
                            KelebihanPlafon = m.KelebihanPlafon,
                            IuranPerAnggota = m.IuranPerAnggota,
                            TglJatuhTempo = m.TglJatuhTempo,
                            JenisFormularium = m.JenisFormularium,
                            MaxRawatOpname1 = m.MaxHariRawatPerOpname,
                            MaxRawatOpname2 = m.MaxHariRawatPerTahun,
                            MaxICUOpname1 = m.MaxICUPerOpname,
                            MaxICUOpname2 = m.MaxICUPerTahun,
                            MaxNilaiRawatInap = m.MaxRIRupiahPerTahun,
                            TarifYangDigunakanUp = m.TarifUp,
                            TarifYangDigunakanDiskon = m.TarifDiscount,
                            DiskonLangsungKePasien = m.DiskonLangsungKePasien,
                            HargaObatDigunakan = m.HargaObatDigunakan,
                            UpObat = m.ObatUp,
                            DiskonObat = m.ObatDiscount,
                            JenisPembayaran = m.IDBayar,
                            JenisPembayaranYangDitanggung = m.PersenDitanggung,
                            PaketKerjasama = m.PaketKerjasama,
                            Aktif = m.Active,
                            SembunyikanObat = m.HidenObatNonFormularium,
                            PaketPemeriksaan = m.PaketKerjasama
                        },
                        DetailPlafon = d_DetailPlafon.ConvertAll(x => new
                        {
                            KategoriPlafon = x.KategoriPlafon,
                            Ditanggung = x.Ditanggung,
                            NilaiPlafon = x.NilaiPlafon

                        }),
                        Formularium = d_Formularium.ConvertAll(x => new
                        {
                            Barang_ID = x.Barang_ID,
                            Deskripsi = "",
                            Satuan = "",
                            Subkategori = "",
                            Formularium = "",
                            Ditanggung = ""

                        }),
                        Diskon = d_Diskon.ConvertAll(x => new
                        {
                            IDDiskon = x.IDDiscount,
                            NamaDiskon = x.NamaDiscount,
                            Disc = x.Nilai
                        }),
                        SectionDitanggung = d_SectionDitanggung.ConvertAll(x => new
                        {
                            SectionID = x.SectionID,
                            SectionName = "",
                            Ditanggung = x.Ditanggung
                        }),
                        JasaExectionDiskon = d_JasaExectionDiskon.ConvertAll(x => new
                        {
                            JasaID = x.JasaID,
                            JasaName = x.JasaName
                        }),
                        RasioObat = d_RasioObat.ConvertAll(x => new
                        {
                            Spesialis = x.SpesialisName,
                            Subspesialis = x.SubSpesialisName,
                            Nilai = x.NilaiRasio,
                            Blok = x.Blok
                        }),
                        Markup = d_Markup.ConvertAll(x => new
                        {
                            GroupJasa = x.GroupJasaName,
                            Markup = x.Kenaikan_Prosen,
                            Keterangan = x.Keterangan
                        }),
                        DataPaketObat = d_DataPaketObat.ConvertAll(x => new
                        {
                            JenisObat = x.KelompokJenisObat
                        }),
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K - U P

        [HttpPost]
        public string List_Perusahaan(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_GetDataPerusahaan.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Kode_Customer.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Customer_ID = m.Customer_ID,
                        Kode_Customer = m.Kode_Customer,
                        Nama_Customer = m.Nama_Customer

                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K - U P DISCOUNT

        [HttpPost]
        public string List_Discount(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_GetDiscountKontrakKerjasama.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.IDDiscount.Contains(x.Value) ||
                                y.NamaDiscount.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        IDDiscount = m.IDDiscount,
                        NamaDiscount = m.NamaDiscount,
                        Disc = '0',
                        Tipe = "Langsung Ke Pasien",
                        Rincian = '1'
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K - U P FORMULARIUM

        [HttpPost]
        public string List_Formularium(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_GetFormulariumKontrakKerjasama.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Kode_Barang.Contains(x.Value) ||
                                y.Nama_Barang.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Barang_ID = m.Barang_ID,
                        Kode_Barang = m.Kode_Barang,
                        Nama_Barang = m.Nama_Barang,
                        Kode_Satuan_Beli = m.Kode_Satuan_Beli,
                        SubKategori = m.SubKategori,
                        Formularium = m.Formularium
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K - U P SECTION TDK DISKON

        [HttpPost]
        public string Section_Tdk_Diskon(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_GetSectionKontrakKerjasama.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.SectionID.Contains(x.Value) ||
                                y.SectionName.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        SectionID = m.SectionID,
                        SectionName = m.SectionName
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K - U P JASA EXPECTION

        [HttpPost]
        public string Jasa_Expection(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_GetJasaExceptionDisc.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.JasaID.Contains(x.Value) ||
                                y.JasaName.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        JasaID = m.JasaID,
                        JasaName = m.JasaName
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== L O O K - U P RASIOOBAT

        [HttpPost]
        public string Rasio_Obat(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_GetRasioObatKontrak.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.SpesialisID.Contains(x.Value) ||
                                y.SubSpesialisID.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        SpesialisID = m.SpesialisID,
                        SubSpesialisID = m.SubSpesialisID,
                        NilaiRasio = m.NilaiRasio,
                        Blok = m.Blok
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}