﻿using Marketing.Entities.SIM;
using Marketing.Helper;
using Marketing.Models.Setup;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Marketing.Controllers.Setup
{
    [Authorize(Roles = "Marketing")]
    public class JenisPasienController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_ListJenisPasien.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.JenisKerjasama.Contains(x.Value) ||
                                y.Tipe.Contains(x.Value));
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.JenisKerjasamaID,
                        Jenis = m.JenisKerjasama,
                        Tipe = m.Tipe,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, JenisPasienModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        int id = 0;
                        if (_process == "CREATE")
                        {
                            id = s.MR_InsertJenisPasien(
                                model.JenisPasien,
                                model.Tipe,
                                model.JenisPembayaran,
                                model.Kerjasama,
                                model.MarkupKITAS,
                                model.MarkupNonKITAS
                            );
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            s.MR_UpdateJenisPasien(
                                model.Id,
                                model.JenisPasien,
                                model.Tipe,
                                model.JenisPembayaran,
                                model.Kerjasama,
                                model.MarkupKITAS,
                                model.MarkupNonKITAS
                            );

                            s.SaveChanges();
                            id = model.Id;
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupJenisPasien-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(int id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetJenisPasien.FirstOrDefault(x => x.JenisKerjasamaID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.JenisKerjasamaID,
                            JenisPasien = m.JenisKerjasama,
                            Tipe = m. Tipe,
                            JenisPembayaran = m.JenisPembayaran,
                            Kerjasama = m.Kerjasama,
                            MarkupKITAS = m.MarkupKitas,
                            MarkupNonKITAS= m.MarkupNonKitas
                        }
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}