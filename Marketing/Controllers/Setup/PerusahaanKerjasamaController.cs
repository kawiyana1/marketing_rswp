﻿using Marketing.Entities.SIM;
using Marketing.Helper;
using Marketing.Models.Setup;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;

namespace Marketing.Controllers.Setup
{
    [Authorize(Roles = "Marketing")]
    public class PerusahaanKerjasamaController : Controller
    {
        #region ===== L I S T

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string List(string orderby, bool orderbytype, int pagesize, int pageindex, List<FilterModel> filter)
        {
            try
            {
                int totalcount;
                using (var s = new SIMEntities())
                {
                    var proses = s.MR_ListPerusahaanKerjasama.AsQueryable();
                    foreach (var x in filter)
                    {
                        if (x.Key == "All" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Kode_Customer.Contains(x.Value) ||
                                y.Nama_Customer.Contains(x.Value) ||
                                y.Alias.Contains(x.Value));
                        }
                        else if (x.Key == "Kategori" && !string.IsNullOrEmpty(x.Value))
                        {
                            proses = proses.Where(y =>
                                y.Kode_Kategori == x.Value);
                        }
                    }
                    totalcount = proses.Count();
                    var models = proses.OrderBy($"{orderby} {(orderbytype ? "ASC" : "DESC")}").Skip((pageindex) * pagesize).Take(pagesize).ToList();
                    var r = models.ConvertAll(m => new
                    {
                        Id = m.Customer_ID,
                        Kode = m.Kode_Customer,
                        Nama = m.Nama_Customer,
                        Alias = m.Alias,
                    });
                    return JsonConvert.SerializeObject(new { IsSuccess = true, Data = r, TotalCount = totalcount });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion

        #region ===== S E T U P

        [HttpPost]
        public string Save(string _process, PerusahaanKerjasamaModel model)
        {
            using (var s = new SIMEntities())
            {
                using (var dbContextTransaction = s.Database.BeginTransaction())
                {
                    try
                    {
                        var userid = User.Identity.GetUserId();
                        int id = 0;
                        if (_process == "CREATE")
                        {
                            if (model.Detail == null) model.Detail = new List<SetupPerusahaanKerjasamaDetailModel>();
                            //if (model.Detail.GroupBy(x => x.Nama).Where(x => x.Count() > 1).Count() > 0)
                            //    throw new Exception("Nama Detail Kontak tidak boleh sama");

                            id = s.MR_InsertHeaderPerusahaanKerjasama(
                                model.NamaCustomer,
                                model.Alias,
                                model.Alamat1,
                                model.Kategori,
                                model.AlamatClaim1,
                                model.AlamatClaim2,
                                model.AlamatClaim3,
                                model.KodePos,
                                model.NoTelp1,
                                model.NoTelp2,
                                model.NoTelp3,
                                model.TypePembayaran,
                                model.TemPembayaran,
                                model.NoAkun,
                                model.Active,
                                model.NoFax,
                                model.Email,
                                model.Website,
                                model.NoNPWP,
                                model.MataUang,
                                model.BatasKredit
                            );
                            foreach (var x in model.Detail)
                            {
                                s.MR_InsertDetailPerusahaanKerjasama(
                                    (short)id,
                                    x.Nama,
                                    x.Alamat,
                                    x.TelpKantor,
                                    x.TelpRumah,
                                    x.HP,
                                    x.Fax,
                                    x.TglLahir,
                                    x.Jabatan,
                                    x.Email,
                                    x.Keterangan
                                );
                            }
                            s.SaveChanges();
                        }
                        else if (_process == "EDIT")
                        {
                            if (model.Detail == null) model.Detail = new List<SetupPerusahaanKerjasamaDetailModel>();
                            //if (model.Detail.GroupBy(x => x.Nama).Where(x => x.Count() > 1).Count() > 0)
                            //    throw new Exception("Nama Detail Kontak tidak boleh sama");

                            s.MR_UpdateHeaderPerusahaanKerjasama(
                                model.KodeCustomer,
                                model.NamaCustomer,
                                model.Alias,
                                model.Alamat1,
                                model.Kategori,
                                model.AlamatClaim1,
                                model.AlamatClaim2,
                                model.AlamatClaim3,
                                model.KodePos,
                                model.NoTelp1,
                                model.NoTelp2,
                                model.NoTelp3,
                                model.TypePembayaran,
                                model.TemPembayaran,
                                model.NoAkun,
                                model.Active,
                                model.NoFax,
                                model.Email,
                                model.Website,
                                model.NoNPWP,
                                model.MataUang,
                                model.BatasKredit
                            );

                            var d = s.MR_GetDetailPerusahaanKerjasama.Where(x => x.Customer_ID == model.Id).ToList();
                            foreach (var x in d)
                            {
                                var _d = model.Detail.FirstOrDefault(y => y.Id == x.KontakPerson_ID);
                                //delete
                                if (_d == null) s.MR_DeleteDetailPerusahaanKerjasama((short)model.Id, x.KontakPerson_ID);
                            }
                            foreach (var x in model.Detail)
                            {
                                var _d = d.FirstOrDefault(y => y.KontakPerson_ID == x.Id);
                                if (_d == null)
                                {
                                    // new
                                    s.MR_InsertDetailPerusahaanKerjasama(
                                        (short)model.Id,
                                        x.Nama,
                                        x.Alamat,
                                        x.TelpKantor,
                                        x.TelpRumah,
                                        x.HP,
                                        x.Fax,
                                        x.TglLahir,
                                        x.Jabatan,
                                        x.Email,
                                        x.Keterangan
                                    );
                                }
                                else
                                {
                                    // edit
                                    s.MR_UpdateDetailPerusahaanKerjasama(
                                        _d.KontakPerson_ID,
                                        x.Nama,
                                        x.Alamat,
                                        x.TelpKantor,
                                        x.TelpRumah,
                                        x.HP,
                                        x.Fax,
                                        x.TglLahir,
                                        x.Jabatan,
                                        x.Email,
                                        x.Keterangan
                                    );
                                }

                            }
                            s.SaveChanges();
                            id = model.Id;
                        }

                        var userActivity = new UserActivityModel(Request, User.Identity.GetUserId())
                        {
                            Activity = $"SetupPerusahaanKerjasama-{_process}; id:{id};".ToLower()
                        };
                        UserActivity.InsertUserActivity(userActivity);
                        dbContextTransaction.Commit();

                        return JsonConvert.SerializeObject(new { IsSuccess = true, Id = id });
                    }
                    catch (SqlException ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                    catch (Exception ex) { dbContextTransaction.Rollback(); return HConvert.Error(ex); }
                }
            }
        }

        [HttpPost]
        public string Detail(int id)
        {
            try
            {
                using (var s = new SIMEntities())
                {
                    var m = s.MR_GetHeaderPerusahaanKerjasama.FirstOrDefault(x => x.Customer_ID == id);
                    if (m == null) throw new Exception("Data tidak ditemukan");
                    var d = s.MR_GetDetailPerusahaanKerjasama.Where(x => x.Customer_ID == m.Customer_ID).ToList();
                    return JsonConvert.SerializeObject(new
                    {
                        IsSuccess = true,
                        Data = new
                        {
                            Id = m.Customer_ID,
                            KodeCustomer = m.Kode_Customer,
                            NamaCustomer = m.Nama_Customer,
                            Alias = m.Alias,
                            Kategori = m.Kode_Kategori,
                            Alamat1 = m.Alamat_1,
                            AlamatClaim1 = m.Claim_address1,
                            AlamatClaim2 = m.Claim_address2,
                            AlamatClaim3 = m.Claim_address3,
                            KodePos = m.Kode_Pos,
                            NoTelp1 = m.No_Telepon_1,
                            NoTelp2 = m.No_Telepon_2,
                            NoTelp3 = m.No_Telepon_3,
                            NoFax = m.No_Fax,
                            Email = m.Alamat_Email,
                            Website = m.Alamat_Website,
                            NoNPWP = m.No_NPWP,
                            MataUang = m.MataUang,
                            BatasKredit = m.Batas_Kredit,
                            TypePembayaran = m.Type_Pembayaran,
                            TemPembayaran = m.Term_Pembayaran,
                            IdAkun = m.Akun_ID,
                            NoAkun = m.Akun_No,
                            Active = m.Active,
                            AkunNama = m.Akun_Name
                        },
                        Detail = d.ConvertAll(x => new
                        {
                            Nama = x.Nama,
                            Alamat = x.Alamat,
                            TelpKantor = x.No_Telepon_Kantor,
                            TelpRumah = x.No_Telepon_Rumah,
                            HP = x.No_Handphone,
                            Fax = x.Fax,
                            TglLahir = x.Tgl_Lahir?.ToString("yyyy-MM-dd"),
                            Jabatan = x.Jabatan,
                            Email = x.Alamat_Email,
                            Keterangan = x.Keterangan,
                        })
                    });
                }
            }
            catch (SqlException ex) { return HConvert.Error(ex); }
            catch (Exception ex) { return HConvert.Error(ex); }
        }

        #endregion
    }
}