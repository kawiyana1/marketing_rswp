﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Marketing.Models.Kerjasama
{
    public class SetupTarifKerjasamaModel
    {
        public short CUstomerKerjasamaID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Copay { get; set; }
        public short MaxHariRawatPerOpname { get; set; }
        public short MaxHariRawatPerTahun { get; set; }
        public string KelebihanPlafon { get; set; }

        public List<SetupJasaTarifKerjasamaDetailModel> DetailJasa { get; set; }
        public List<SetupPlafonDetailModel> DetailPlafon { get; set; }
        public List<SetupFormulariumDetailModel> DetailFormularium { get; set; }
    }
    public class SetupJasaTarifKerjasamaDetailModel
    {
        public decimal CUstomerKerjasamaID { get; set; }
        public decimal ListHargaID { get; set; }
        public decimal ListHargaIDLama { get; set; }
        public decimal HargaLama { get; set; }
        public decimal HargaBaru { get; set; }
        public bool Included { get; set; }
        public DateTime TglPerubahanHarga { get; set; }
        public bool HonorDefault { get; set; }
        public double Honor { get; set; }

        //public List<SetupKontrakMCUDetailKomponenModel> Detail { get; set; }
    }

    public class SetupPlafonDetailModel
    {
        public decimal CustomerKerjasamaID { get; set; }
        public string KategoriPlafon { get; set; }
        public bool Ditanggung { get; set; }
        public decimal NilaiPlafon { get; set; }

    }

    public class SetupFormulariumDetailModel
    {
        public decimal CustomerKerjasamaID { get; set; }
        public int BarangID { get; set; }
        public short JenisKerjasamaID { get; set; }
        public bool Include { get; set; }
        public bool Ditanggung { get; set; }

    }

    //public class SetupKontrakMCUDetailKomponenModel
    //{
    //    public string MCUID { get; set; }
    //    public string JasaID { get; set; }
    //    public string KomponenName { get; set; }
    //    public decimal Harga { get; set; }
    //}
    
}