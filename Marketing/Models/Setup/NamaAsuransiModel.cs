﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Marketing.Models.Setup
{
    public class NamaAsuransiModel
    {
        public string Id { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string KontakPerson { get; set; }
        public string TypeAsuransi { get; set; }
    }
}