﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Marketing.Models.Setup
{
    public class TypeAsuransiModel
    {
        public string Id { get; set; }
        public string Deskripsi { get; set; }
        public string Catatan { get; set; }
        public List<TypeAsuransiDetailModel> Detail { get; set; }
    }

    public class TypeAsuransiDetailModel 
    {
        public int TipePasien { get; set; }
        public decimal RangeMin { get; set; }
        public decimal? RangeMax { get; set; }
        public double? ProsenUp { get; set; }
    }
}