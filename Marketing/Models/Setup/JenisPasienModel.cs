﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Marketing.Models.Setup
{
    public class JenisPasienModel
    {
        public int Id { get; set; }
        public string JenisPasien { get; set; }
        public string Tipe { get; set; }
        public int? JenisPembayaran { get; set; }
        public bool Kerjasama { get; set; }
        public double? MarkupKITAS { get; set; }
        public double? MarkupNonKITAS { get; set; }
    }
}